Custom firmware for the BITalino R-IoT to operate along with a HC-SR04 ultrasonic sensor.

### Circut

Connect the correspoding pins of the microcontroller and sensor:

| R-IoT | HC-SR04 |
| ------ | ------ |
| VCC (3.3V) | +5V |
| GND | GND |
| OUT | TRIG |
| IN | ECHO |

In principle, you may need to apply a voltage divider to operate the sensor at 3.3V. Some non-genuine models can work without this.

### Configuration

The sample period will match the R-IoT configuration. We used 100ms (10Hz) as our default.

Additional informaiton can be found in the quickstart guide: [https://bitalino.com/storage/uploads/media/r-iot-configuration-guide.pdf](https://bitalino.com/storage/uploads/media/r-iot-configuration-guide.pdf)

### Data

Proximity Data is included in the default OSC message with the `/<id>/raw` address. The specific reading will appear under the the `A1` label.

